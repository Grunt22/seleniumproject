package Selenium;

import org.testng.annotations.Test;

public class ExampleNG {

	
	@Test(priority=0)
	public void one() {
		System.out.println("First");
	}
	
	@Test(priority=1)
	public void two() {
		System.out.println("second");
	}
	
	@Test(priority=2)
	public void Three() {
		System.out.println("Third");
	}	
	
	@Test
	public void Four() {
		System.out.println("Fourth");
	}
	
	
	@Test
	public void Five() {
		System.out.println("Fifth");
	}
	
}
